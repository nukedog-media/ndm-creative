# NDM Creative
is written from scratch.

NDM Creative is a one page (or maybe a Frontpage/Landingpage) like Website. The build contains a few third party tools, as well as the code we write. There may also be snippets of code used that have been found on some forums around the web.

----

_NOTE: The begin of this project started with a [Udemy Course](). After which I continue working on it, making changes as I see fit._

## 3rd Party Tools Utilized
The following third party tools are an essential part of this project.
* Twitter Bootstrap
* FontAwesome Icons
* Google Fonts
* Owl Carousel

## Features
* Page devided into sections
  * Home
  * About
  * Team
  * Statement
  * Services
  * Work (Portfolio)
  * Testimoials
  * Pricing
  * Stats
  * Clients
  * Blog
  * Contact
  * Map
  * Footer

* CSS & JS Aninations
  * Sliding Images
  * Color Changes
  * Vertical Text
  * Parallax Like Background Image
  * 
  * A lot more...
  











