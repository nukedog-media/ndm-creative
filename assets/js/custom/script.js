/* =========================================================
                        PRELOADER
========================================================= */
$(window).on('load', function () { // fully loads whole site!
    $('#status').fadeOut();

    $('#preloader').delay(350).fadeOut('slow');
});


/* =========================================================
                           TEAM
        https://owlcarousel2.github.io/OwlCarousel2/
========================================================= */
$(function () {
    $("#team-members").owlCarousel({
        items: 2,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left fa-2x"></i>', '<i class="fa fa-angle-right fa-2x"></i>']
    });
});


/* =========================================================
                    PROGRESS BARS
            http://imakewebthings.com/waypoints/
========================================================= */
$(function () {
    $("#progress-elements").waypoint(function () {
        $(".progress-bar").each(function () {
            $(this).animate({
                width: $(this).attr("aria-valuenow") + "%"
            }, 2000);
        });
        this.destroy();
    }, {
        offset: 'bottom-in-view'
    });
});


/* =========================================================
                    RESPONSIVE TABS
        http://jellekralt.github.io/Responsive-Tabs/
========================================================= */
$(function () {
    $('#services-tabs').responsiveTabs({
        animation: 'slide'
    });
});


/* =========================================================
                    PORTFOLIO : ISOTOPE
                https://isotope.metafizzy.co/
========================================================= */
$(window).on('load', function () {

    // Initialize Isotope
    $("#isotope-container").isotope({});

    // filter items on button click
    $("#isotope-filters").on('click', 'button', function () {

        // get filter value
        var filterValue = $(this).attr('data-filter');

        // filter portfolio
        $("#isotope-container").isotope({
            filter: filterValue
        });

        // active button
        $("#isotope-filters").find('.active').removeClass('active');
        $(this).addClass('active');
    });
});


/* =========================================================
                    Magnific Popup
        https://github.com/dimsemenov/Magnific-Popup        
========================================================= */
$(function () {
    $("#portfolio-wrapper").magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true
        }
    });
});


/* =========================================================
                        Testimonials
        https://owlcarousel2.github.io/OwlCarousel2/
========================================================= */
$(function () {
    $("#testimonial-slider").owlCarousel({
        items: 1,
        autoplay: false,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left fa-2x"></i>', '<i class="fa fa-angle-right fa-2x"></i>']
    });
});


/* =========================================================
                        Stats
        https://github.com/ciromattia/jquery.counterup
========================================================= */
$(function () {
    $('.counter').counterUp({
        delay: 20,
        time: 3000
    });
});


/* =========================================================
                        Clients
========================================================= */
$(function () {
    $("#clients-list").owlCarousel({
        items: 6,
        autoplay: false,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left fa-2x"></i>', '<i class="fa fa-angle-right fa-2x"></i>']
    });
});


/* =============================================================================
                        Google Map

            Needs API key. May also cause costs.
https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
============================================================================= */
$(window).on('load', function () {
    // Map Variables
    var addressString = '460  W 42nd St, New York, NY 10036, USA';
    var myLatlng = {
        lat: 40.759440,
        lng: -73.995160
    };

    // 1. Render Map
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 11,
        center: myLatlng
    });

    // 2. Add Marker, positioned at myLatlng
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Click to see address.'
    });

    // 3. Add Info Window
    var infowindow = new google.maps.InfoWindow({
        content: addressString,
    });

    marker.addListener("click", () => {
        infowindow.open({
            anchor: marker,
            map,
            shouldFocus: false,
        });
    });

});


/* =========================================================
                        NAVIGATION
========================================================= */
/* Hide & Show White Navbar */
$(function() {

    // show/hide nav on pageload
    showHideNav();

    $(window).scroll(function(){

        // show/hide nav on scroll
        showHideNav();

    });

    function showHideNav() {
        if( $(window).scrollTop() > 50 ) {
            // show white nav
            $("nav").addClass("white-nav-top");
            // show dark logo
            $(".navbar-brand img").attr("src", "assets/img/logo/logo-dark.png");
            // show back to top btn
            $("#back-to-top").fadeIn();
        } else {
            // hide white nav
            $("nav").removeClass("white-nav-top");
            $(".navbar-brand img").attr("src", "assets/img/logo/logo.png");
            // show back to top btn
            $("#back-to-top").fadeOut();
        }
    }

});

// smooth scrolling
$(function () {

    $("a.smooth-scroll").click(function (event) {

        event.preventDefault();

        // get section id
        var section_id = $(this).attr("href");

        $("html, body").animate({
            scrollTop: $(section_id).offset().top - 64
        }, 1250, "easeInOutExpo");

    });

});














